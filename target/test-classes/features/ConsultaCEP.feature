# language: pt
@ConsultaCep
Funcionalidade: Validar CEP

  @CepValido
  Esquema do Cenario: Validar CEP - Valido
    Dado que o servico for consultado com sucesso para o cep "<cep>"
    Entao e retornado CEP, logradouro, complemento, bairro, localidade, uf e ibge do "<cep>"

    Exemplos: 
      | cep      |
      | 91060900 |

  @CepInvalido
  Esquema do Cenario: Validar CEP - Invalido
    Dado que o servico retornar o status de erro para o cep "<cep>"
    Entao valido que o cep "<cep>" inserido foi invalido

    Exemplos: 
      | cep      |
      | 12345677 |

  @CepFormatoInvalido
  Esquema do Cenario: Validar CEP - Formato Invalido
    Dado que o servico retornar o status de erro por formatacao para o cep "<cep>"
    Entao valido que o cep "<cep>" inserido esta com formato incorreto

    Exemplos: 
      | cep       |
      | 000008-00 |
      
