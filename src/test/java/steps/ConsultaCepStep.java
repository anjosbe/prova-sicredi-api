package steps;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.E;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import com.opencsv.CSVWriter;

import pages.ConsultaCEPPage;

public class ConsultaCepStep {

	@Dado("^que o servico for consultado com sucesso para o cep \"([^\"]*)\"$")
	public void que_o_servico_for_consultado_com_sucesso_para_o_cep(String cep) throws Throwable {
		new ConsultaCEPPage().getValidaCepSucesso(cep);
	}

	@Dado("^que o servico retornar o status de erro para o cep \"([^\"]*)\"$")
	public void que_o_servico_retornar_o_status_de_erro_para_o_cep(String cep) throws Throwable {
		new ConsultaCEPPage().getValidaCepIncorreto(cep);
	}

	@Dado("^que o servico retornar o status de erro por formatacao para o cep \"([^\"]*)\"$")
	public void que_o_servico_retornar_o_status_de_erro_por_formatacao_para_o_cep(String cep) throws Throwable {
		new ConsultaCEPPage().getValidaCepFormatoIncorreto(cep);
	}

	@Entao("^valido que o cep \"([^\"]*)\" inserido foi invalido$")
	public void valido_que_o_cep_inserido_foi_invalido(String cep) throws Throwable {
		new ConsultaCEPPage().getRetornaStatus500(cep);
	}

	@Entao("^valido que o cep \"([^\"]*)\" inserido esta com formato incorreto$")
	public void valido_que_o_cep_inserido_esta_com_formato_incorreto(String cep) throws Throwable {
		new ConsultaCEPPage().getRetornaStatus400(cep);
	}

	@Entao("^e retornado CEP, logradouro, complemento, bairro, localidade, uf e ibge do \"([^\"]*)\"$")
	public void e_retornado_CEP_logradouro_complemento_bairro_localidade_uf_e_ibge(String cep) throws Throwable {
		new ConsultaCEPPage().getRetornaElementos(cep, null);
	}

}
