package pages;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import com.opencsv.CSVWriter;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import static io.restassured.RestAssured.*;

public class ConsultaCEPPage {

	public ConsultaCEPPage() {
	}

	public void getValidaCepSucesso(String cep) {
		given().get("https://viacep.com.br/ws/" + cep + "/json/").then().assertThat().statusCode(200);
	}

	public void getValidaCepIncorreto(String cep) {
		given().get("https://viacep.com.br/ws/" + cep + "/json/").then().assertThat().statusCode(200);
	}

	public void getValidaCepFormatoIncorreto(String cep) {
		given().get("https://viacep.com.br/ws/" + cep + "/json/").then().assertThat().statusCode(400);

	}

	public void getRetornaElementos(String cep, String args[]) throws IOException {
		List<String[]> linhas = new ArrayList<>();

		Response response = given().get("https://viacep.com.br/ws/" + cep + "/json/");
		JsonPath jsonPathEvaluator = response.jsonPath();

		System.out.println("O CEP: " + jsonPathEvaluator.get("cep"));
		System.out.println("O Lograudouro: " + jsonPathEvaluator.get("logradouro"));
		System.out.println("O Complemento: " + jsonPathEvaluator.get("complemento"));
		System.out.println("O Bairro: " + jsonPathEvaluator.get("bairro"));
		System.out.println("A Localidade: " + jsonPathEvaluator.get("localidade"));
		System.out.println("O Estado: " + jsonPathEvaluator.get("uf"));
		System.out.println("E o IBGE: " + jsonPathEvaluator.get("ibge"));

		// Gerar CSV
		String[] cabecalho = { "cep", "logradouro", "complemento", "bairro", "localidade", "UF", "IBGE" };

		linhas.add(new String[] { jsonPathEvaluator.get("cep"), jsonPathEvaluator.get("logradouro"),
				jsonPathEvaluator.get("complemento"), jsonPathEvaluator.get("bairro"),
				jsonPathEvaluator.get("localidade"), jsonPathEvaluator.get("uf"), jsonPathEvaluator.get("ibge") });

		Writer writer = Files.newBufferedWriter(Paths.get("cepvalido.csv"));
		CSVWriter csvWriter = new CSVWriter(writer);

		csvWriter.writeNext(cabecalho);
		csvWriter.writeAll(linhas);

		csvWriter.flush();
		writer.close();
	}

	public void getRetornaStatus400(String cep) throws IOException {
		List<String[]> linhas = new ArrayList<>();

		String response = given().get("https://viacep.com.br/ws/" + cep + "/json/").getStatusLine();
		System.out.println("O status da requisi��o �: " + response);

		// Gerar CSV
		String[] cabecalho = { "cep", "status", "descrisicao" };

		linhas.add(new String[] { cep, "400", "Bad Request" });

		Writer writer = Files.newBufferedWriter(Paths.get("cepstatus400.csv"));
		CSVWriter csvWriter = new CSVWriter(writer);

		csvWriter.writeNext(cabecalho);
		csvWriter.writeAll(linhas);

		csvWriter.flush();
		writer.close();
	}

	public void getRetornaStatus500(String cep) throws IOException {
		List<String[]> linhas = new ArrayList<>();

		Response response = given().get("https://viacep.com.br/ws/" + cep + "/json/");
		String statusReq = response.getStatusLine();
		JsonPath jsonPathEvaluator = response.jsonPath();
		System.out.println("O status da requisicao: " + statusReq);
		String erro = jsonPathEvaluator.get("erro").toString();
		System.out.println("Porem o erro retorna: " + erro + "\nSendo assim o status real �: 500");

		// Gerar CSV
		String[] cabecalho = { "cep", "status", "erro (true/false)" };

		linhas.add(new String[] { cep, "500", erro });

		Writer writer = Files.newBufferedWriter(Paths.get("cepINVALIDO.csv"));
		CSVWriter csvWriter = new CSVWriter(writer);

		csvWriter.writeNext(cabecalho);
		csvWriter.writeAll(linhas);

		csvWriter.flush();
		writer.close();
	}

}
